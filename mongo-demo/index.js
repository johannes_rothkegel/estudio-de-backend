const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/mongo-exercises')
    .then(() => console.log('Connected to MongoDB...'))
    .catch((err) => console.error('Could not connect to MongoDB..',err));

const courseSchema = new mongoose.Schema({
    name: String,
    author: String,
    tags: [String],
    date: { type: Date, default: Date.now},
    isPublished: Boolean
});

const Course = mongoose.model('Course', courseSchema);

async function createCourse(){

    const course = new Course({
        name: 'Regular Course',
        author: 'Johannes',
        tags: ['angular','frontend'],
        isPublished: true
    });
    const result = await course.save();
    console.log(result);
}

async function getCourses(){
    const courses = await Course
        .find({author: 'Johannes', isPublished: true})
        .limit(10)
        .sort({name: 1})
        .select({name: 1 , tags: 1});
        console.log(courses);
}


async function updateCourse(id){
    const course = await Course.findByIdAndUpdate(id,{
        $set: {
            author: 'Jason',
            isPublished: false
        }
    }, {new: true });
    console.log(course);
}


updateCourse('5b3af1065eb6462090c63c2a');