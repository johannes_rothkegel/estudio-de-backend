const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/mongo-exercises')
    .then(() => console.log('Connected to MongoDB...'))
    .catch((err) => console.error('Could not connect to MongoDB..',err));

const courseSchema = new mongoose.Schema({
    name: String,
    author: String,
    tags: [String],
    date: { type: Date, default: Date.now},
    isPublished: Boolean
});

const Course = mongoose.model('Course', courseSchema);


async function getCourses(){
    const courses = await Course
        .find({isPublished: true})
        .or( [{price:{$gte: 15}}, {name: /.*by.*/i}])
        .sort({price: -1})
        .select('name author price');
    console.log(courses);
}

async function updateCourse(id){
    const course = await Course.findOne({_id: id});
    if(!course) return console.log('Id not found..');

    course.isPublished = true;
    course.author = 'Another Author';

    const result = await course.save();
    console.log(result);
}

updateCourse("5a68fe2142ae6a6482c4c9cb");