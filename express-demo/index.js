const Joi = require('joi');
const logger = require('./middleware/logger');
const express = require('express');
const app = express();
const courses = require('./routes/courses');
const intro = require('./routes/intro');

app.set('view engine','pug');
app.set('views', './views'); //default

app.use(express.json());
app.use('/api/courses',courses);
app.use('/',intro);
app.use(logger);


// PORT
const port =process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}....`));
